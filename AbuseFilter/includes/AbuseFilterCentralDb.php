<?php

use MediaWiki\MediaWikiServices;

/**
 * Database factory class, this will determine whether to use the main database
 * or an external database defined in configuration file
 */
class AbuseFilterCentralDb {
	/**
	 * Singleton instance of this class.
	 *
	 * @var object
	 */
	 static private $instance = null;

	/**
	 * Central database connection.
	 * This will get set to false if a central database is not configured.
	 *
	 * @var array
	 */
	 private $dbs = [];

	/**
	 * Central cluster.
	 *
	 * @var string
	 */
	 private $cluster = null;

	/**
	 * Central database.
	 *
	 * @var string
	 */
	 private $database = null;

	 /**
	  * Main Constructor
	  *
	  * @access public
	  * @param string Cluster name from $wgExternalClusters.
	  * @param string Database name for the central database.
	  * @return void
	  */
	 public function __construct( $cluster, $database ) {
		$this->cluster = $cluster;
		$this->database = $database;
	 }

	 /**
	  * Return a singleton instance of this class.
	  *
	  * @access public
	  * @return object AbuseFilterDbFactory
	  */
	 static public function getInstance() {
		if ( self::$instance === null ) {
			$config = MediaWikiServices::getInstance()->getConfigFactory()::getDefaultInstance()->makeConfig( 'main' );
			$abuseFilterCentralCluster = $config->get( 'AbuseFilterCentralCluster' );
			$abuseFilterCentralDB = $config->get( 'AbuseFilterCentralDB' );

			self::$instance = new self( $abuseFilterCentralCluster, $abuseFilterCentralDB );
		}

		return self::$instance;
	 }

	/**
	 * Get the central database filter store.
	 *
	 * @access public
	 * @param integer $dbtype either DB_MASTER or DB_REPLICA.
	 * @return DatabaseBase|false DatabaseBase or false on connection error.
	 */
	public function getCentralDb( $dbtype = DB_MASTER ) {
		if ( isset( $this->dbs[$dbtype] ) && $this->dbs[$dbtype] !== null ) {
			return $this->dbs[$dbtype];
		}

		$db = false;
		if ( $this->isUsingCentralDb() ) {
			if ( $this->isUsingCentralCluster() ) {
				$db = MediaWikiServices::getInstance()->getDBLoadBalancerFactory()->getExternalLB( $this->cluster )->getConnection( $dbtype, false, $this->database );
			} else {
				$db = wfGetDB( $dbtype, [], $this->database );
			}
		}
		$this->dbs[$dbtype] = $db;
		return $this->dbs[$dbtype];
	}

	/**
	 * Are we using a central cluster?
	 *
	 * @access public
	 * @return boolean
	 */
	public function isUsingCentralCluster() {
		return $this->cluster !== null;
	}

	/**
	 * Are we using a central database?
	 *
	 * @access public
	 * @return boolean
	 */
	public function isUsingCentralDb() {
		return $this->database !== null;
	}
}

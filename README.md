# Third Party Extensions

Third party extensions that we track and update for usage on Gamepedia's Hydra installation.  This project is not intended for consumption outside of Gamepedia unless you really happen to like our set of extensions.

* Only maintainers can make merge requests and commit code.

* We accept issues requesting extension updates to this project, but they will only be done if they are compatible with the MediaWiki version of the hydra repository.(Currently MediaWiki 1.31)